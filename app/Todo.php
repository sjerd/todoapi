<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;

class Todo extends Model
{
    //
    use SoftDeletes;
    protected $table = 'todo';
    protected $fillable = ['title', 'description', 'user_id', 'todo_at', 'done_at'];
    protected $dates = ['deleted_at'];
    private $errors;

    public function validate($data)
    {
        // make a new validator object
        $validator = Validator::make($data, [
          'title' => 'required',
          'description' => 'required',
          'user_id'  => 'required',
          'todo_at'  => 'required',
        ]);

        // check for failure
        if ($validator->fails())
        {
            // set errors and return false
            $this->errors = $validator->errors();
            return false;
        }

        // validation pass
        return true;
    }

    public function errors()
    {
        return $this->errors;
    }
}
