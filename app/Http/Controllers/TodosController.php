<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todo;
use Auth;

class TodosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //
        $todos = Todo::where('user_id', Auth::id())->get();
        $isValid = true;
        return json_encode([
          'todos' => $todos,
          'isValid' => $isValid
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $isValid = true;
        $message = null;
        $new = $request->todo;
        // create a new model instance
        $todo = new Todo;
        $new['user_id'] = Auth::id();
        // attempt validation
        if (!$todo->validate($new)) {
          // failure, get errors
          $isValid = false;
          $message = $todo->errors();
        } else {
          $todo = Todo::create($new);
          $message = 'Todo is Toegevoegd!';
        }

        if($isValid == true) {
        }

        $result = [
          'isValid' => $isValid,
          'message' => $message
        ];

        return json_encode($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $isValid = true;
        $message = null;
        $new = $request->todo;
        // create a new model instance
        $todo = Todo::find($id);
        // attempt validation
        if (!$todo->validate($new)) {
          // failure, get errors
          $isValid = false;
          $message = $todo->errors();
        } else {
          $todo->update($new);
          $message = 'Todo geupdate!';
        }

        if($isValid == true) {
          $todo->save();
        }

        $result = [
          'isValid' => $isValid,
          'message' => $message
        ];

        return json_encode($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $isValid = true;
        $message = 'Todo verwijderd';
        $todo = Todo::find($id);
        $todo->delete();

        $result = [
          'isValid' => $isValid,
          'message' => $message
        ];
    }
}
